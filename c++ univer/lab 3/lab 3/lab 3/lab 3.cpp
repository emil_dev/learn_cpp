﻿#include <iostream>
#include <string>

using namespace std;

class Unit
{

public:

	Unit() {
		cout << "Выделена динамическая память, объекта класса Unit" << endl;
	}

    virtual	~Unit() {
		cout << "Освобождена динамическая память, объекта класса Unit" << endl;
	}

	virtual	void InputInfo() {
		cout << "Название: ";
		cin >> name;
		cout << "Едениц жизни: ";
		cin >> hp;
		cout << "Урон: ";
		cin >> damage;
	}

	virtual	void PrintInfo() {
		cout << "Имя: " << name << endl;
		cout << "Уровень жизней: " << hp << endl;
		cout << "Урон: " << damage << endl;
	}

	virtual	int survivals() = 0;

protected:
	string name;
	int hp;
	int damage;

};

class Archer : public Unit
{
public:

	Archer() {
		cout << "Выделена динамическая память, объекта класса Archer" << endl;
	}

	~Archer() override {
		cout << "Освобождена динамическая память, объекта класса Archer" << endl;
	}

	void InputInfo() override {
		Unit::InputInfo();
		cout << "Радиус атаки: ";
		cin >> AttackRadius;
	}

	void PrintInfo() override {
		Unit::PrintInfo();
		cout << "Радиус атаки:: " << AttackRadius << endl;
	}

	int survivals() override {
		if (hp == 0) {
			cout << "Лучник: " << name << " умер" << endl;
		}
		else {
			cout << "Лучник " << name << " жив. Количество жизней: " << hp << endl;
		}
		return 0;
	}

protected:
	int AttackRadius;
};

class Swordsman : public Unit
{
public:

	Swordsman() {
		cout << "Выделена динамическая память, объекта класса Swordsman" << endl;
	}

	~Swordsman() override {
		cout << "Освобождена динамическая память, объекта класса Swordsman" << endl;
	}

	void InputInfo() override {
		Unit::InputInfo();
		cout << "Длина меча: ";
		cin >> SizeSword;
	}

	void PrintInfo() override {
		Unit::PrintInfo();
		cout << "Длина меча " << SizeSword << endl;
	}

	int survivals() override {
		if (hp == 0) {
			cout << "Мечник: " << name << " умер" << endl;
		}
		else {
			cout << "Мечник " << name << " жив. Количество жизней: " << hp << endl;
		}
		return 0;
	}

protected:
	int SizeSword;
};

int main()
{   
	setlocale(LC_ALL, "rus");

	Unit* uk = new Archer();
	uk->InputInfo();
	uk->PrintInfo();
	uk->survivals();
	delete uk;

	uk = new Swordsman();
	uk->InputInfo();
	uk->PrintInfo();
	uk->survivals();
	delete uk;
	
	return 0;
}
