﻿#include <iostream>
#include <string>

using namespace std;

class Car
{
public:
	Car() { cout << "Cоздан объект класса Car" << endl; }
	virtual	~Car() {
		cout << "Освобождена динамическая память, объекта класса Car" << endl;
	}

	virtual void input() {
		cout << "Марка: ";
		cin >> stamp;
		cout << "Мощность: ";
		cin >> power;
		cout << "Скорость: ";
		cin >> speed;
	}

	virtual void PrintInfo() {
		cout << "Марка: " << stamp << endl;
		cout << "Мощность: " << power << endl;
		cout << "Скорость: " << speed << endl;
	}

	void start() {
		cout << stamp << ": двигатель заведён." << endl;
	}

	void go() {
		cout << stamp << ": врум врум" << endl;
	}

protected:
	string stamp;
	int power;
	int speed;
};

class Transport
{
public:
	Transport() { cout << "Cоздан объект класса Transport" << endl; }
	virtual	~Transport() {
		cout << "Освобождена динамическая память, объекта класса Transport" << endl;
	}

	virtual void input() {
		cout << "Тоннаж: ";
		cin >> weight;
		cout << "Тип: ";
		cin >> type;
	}

	virtual void PrintInfo() {
		cout << "Тоннаж: " << weight << endl;
		cout << "Тип: " << type << endl;
	}

	void go() {
		cout << "брум брум" << endl;
	}

	void DrownOut() {
		cout << "Двигатель заглушен." << endl;
	}

protected:
	int weight;
	string type;
};

class Truck : public Car, public Transport
{
public:
	Truck() { cout << "Cоздан объект производного класса Truck" << endl; };
	~Truck() {
		cout << "Освобождена динамическая память, объекта класса Truck" << endl;
	}
	void input() override {
		Car::input();
		Transport::input();
	}

	void PrintInfo() override {
		Car::PrintInfo();
		Transport::PrintInfo();
	}

	void unhook() {
		cout << stamp << ": прицеп отцеплен" << endl;
	}

};

int main()
{
	setlocale(LC_ALL, "rus");
	/*
		Car toyota;
		Transport nissan;
		Truck Volvo;

		Volvo.input();
		Volvo.PrintInfo();

		Volvo.start();

		Volvo.unhook();
		Volvo.DrownOut();

		Volvo.Car :: go(); */

	Car toyota;
	Transport nissan;

	Truck* trck;
	trck = new Truck();

	trck->input();
	trck->start();
	trck->unhook();

	trck->Car::go();
	trck->Transport::go();

	trck->DrownOut();

	delete trck;

	return 0;
}