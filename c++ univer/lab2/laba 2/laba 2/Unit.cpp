#include <iostream>
#include <string>

using namespace std;

class Unit
{

public:

	Unit(string n, int h, int d) {
		name = n;
		hp = h;
		damage = d;
	}

	Unit() {
		name; hp; damage;
	}

	virtual	void InputInfo() {
		cout << "��������: ";
		cin >> name;
		cout << "������ �����: ";
		cin >> hp;
		cout << "����: ";
		cin >> damage;
	}

	virtual	void Info() {
		cout << "��������: " << name;
		cout << "������ �����: " << hp;
		cout << "����: " << damage;
	}


protected:
	string name;
	int hp;
	int damage;

};

class Archer: public Unit
{
public:
	
	Archer(string n, int h, int d, int ar) : Unit(n, h, d) {
		AttackRadius = ar;
	}

	Archer() : Unit() {
		AttackRadius;
	}

	void InputInfo() override {
		Unit::InputInfo();
		cout << "������ �����: ";
		cin >> AttackRadius;
	}

	void Info() override {
		Unit::Info();
		cout << "����: " << AttackRadius;
	}

	void survivals() {
		setlocale(LC_ALL, "rus");
		if (hp == 0) {
			cout << "������: " << name << " ����";
		}
		else {
			cout << "������: " << name << " ���. ���������� ������: " << hp;
		}
	}

protected:
	int AttackRadius;
};

int main()
{

	setlocale(LC_ALL, "rus");
	Unit parents;
	Archer heir;
	parents.InputInfo();
	heir.InputInfo();
	heir.survivals();

	return 0;
}