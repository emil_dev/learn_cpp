﻿#include <iostream>
#include <string>

using namespace std;

template<class T, class T1>
T Swap(T m, T1 tmp, int i, int j)
{
	int sizeCol = sizeof(m[0]) / sizeof(m[0][0]);
	for (int k = 0; k < sizeCol; k++) {
		T1 tmp = m[i][k];
		m[i][k] = m[j][k];
		m[j][k] = tmp;
	}
	int sizeRows = sizeof(m);
	cout << "Matrix: " << endl;;
	for (int q = 0; q < sizeRows; q++) {
		for (int d = 0; d < sizeCol; d++) {
			cout << m[q][d];
		}
		cout << endl;
	}
	return m;
}



int main()
{
	int matr[4][5] = { {0,7,4,5,5}, {1,4,5,2,3}, {2,2,1,4,1}, {3,8,3,6,2} };

	Swap(matr, 0, 1, 3);

}