﻿#include <iostream>
#include <string>

using namespace std;

class Matrix;

class Vector
{
public:
	void replacingDiag(Matrix& matrix);

private:
	int vect[3] = {2,5,9};
};

class Matrix
{
public:
	friend void Vector::replacingDiag(Matrix& matrix);

private:
	int matr[3][3] = { {6,7,4}, {1,4,5}, {9,2,1} };
};

void Vector::replacingDiag(Matrix& matrix) {
	int sizeVect = sizeof(vect) / sizeof(vect[0]);
	int sizeRows = sizeof(matrix.matr) / sizeof(matrix.matr[0]);
	int sizeCol = sizeof(matrix.matr[0]) / sizeof(matrix.matr[0][0]);

	if((sizeRows==sizeCol) & (sizeCol==sizeVect)){
	for (int i = 0; i < sizeVect; i++) {
		matrix.matr[i][i] = vect[i];
	}
	cout << matrix.matr[0][0];
	cout << vect[2];
	}
	else {
		cout << "Размерность матрицы не позволяет сделать замену главной диагонали вектором";
	}
}

int main()
{
	setlocale(LC_ALL, "rus");

	Matrix matr;
	Vector vec;

	vec.replacingDiag(matr);


}
